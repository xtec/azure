# README

Para instalar librerías y crear una clave ssh:

```pwsh
> curl.exe https://gitlab.com/xtec/azure/-/raw/main/azure.ps1 -O azure.ps1 
> Unblock-File .\azure.ps1
> .\azure.ps1
```

Para crear una sesión:

```pwsh
> Connect-AzAccount
```

Crear una máquina virtual:

``` pwsh
> New-AzResourceGroup -Name xtec -Location westeurope
> New-AzResourceGroupDeployment -ResourceGroupName xtec -TemplateFile vm.bicep -PublicKey $(Get-Content $HOME\.ssh\id_rsa.pub)
> Get-AzPublicIpAddress -ResourceGroupName xtec
> ssh box@<IpAddress>
```

Para eliminar el grupo de recursos:

```
> Remove-AzResourceGroup -Name $ResourceGroupName
```

## TODO

Per desplegar un cluster en Azure només has d'executar aquesta comanda:

```pwsh
azure.ps1
```

Pots instal.lar la versió de Powershell 7 amb aquesta comanda:

```pwsh
$PSVersionTable.PSVersion
winget search Microsoft.PowerShell
winget install --id Microsoft.Powershell --source winget
```

https://learn.microsoft.com/en-us/azure/azure-resource-manager/bicep/deploy-powershell

## 


- [Example: Use the Azure libraries to provision a virtual machine](https://docs.microsoft.com/en-us/azure/developer/python/azure-sdk-example-virtual-machines?tabs=cmd)


## CLI

```sh
ssh -o StrictHostKeyChecking=no azure@


```

## Help

https://github.com/Azure/azure-quickstart-templates


