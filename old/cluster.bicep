@description('Password for the virtual machines.')
@minLength(12)
@secure()
param password string

@description('The location of your cluster.')
param location string = 'westeurope'

@description('The size of your cluster.')
param size int = 4

module vnet 'shared/vnet.bicep' = {
  name: 'vnet'
  params: {
    location: location
    netId: 2
    numberOfSubnets: 2
  }
}

module vms 'shared/vm.bicep' = [for id in range(1, size): {
  name: 'box-${id}'
  params: {
    location: location
    id: id
    subnetId: vnet.outputs.subnets[1].id
    adminPassword: password
    //script: loadTextContent('apache.sh', 'utf-8')
  }
}]

output vnetId string = vnet.outputs.vnetId
