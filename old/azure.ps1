param(
  $file = "cluster.bicep",
  $rg = "box",
  $location = "westeurope"
)

# TODO test pwsh version


# Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser

# https://github.com/andreipintica/Azure-PowerShell-CheatSheet

# Install Azure Module

if (-Not (Get-Command Connect-AzAccount -errorAction SilentlyContinue)) {
  Write-Host("Installing Azure Module")
  Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Scope CurrentUser -Force
  Install-Module -Name Az -Scope CurrentUser -Repository PSGallery -Force
}

# Install Bicep
if (-Not (Get-Command bicep -errorAction SilentlyContinue)) {
  Write-Host("Installing Bicep")
  $installPath = "$env:USERPROFILE\.bicep"
  $installDir = New-Item -ItemType Directory -Path $installPath -Force
  $installDir.Attributes += 'Hidden'
  # Fetch the latest Bicep CLI binary
        (New-Object Net.WebClient).DownloadFile("https://github.com/Azure/bicep/releases/latest/download/bicep-win-x64.exe", "$installPath\bicep.exe")
  # Add bicep to your PATH
  $currentPath = (Get-Item -path "HKCU:\Environment" ).GetValue('Path', '', 'DoNotExpandEnvironmentNames')
  if (-not $currentPath.Contains("%USERPROFILE%\.bicep")) { setx PATH ($currentPath + ";%USERPROFILE%\.bicep") }
  if (-not $env:path.Contains($installPath)) { $env:path += ";$installPath" }
}

if ( -not (Get-AzContext -errorAction SilentlyContinue)) {
  Connect-AzAccount
}


New-AzResourceGroup -Name $rg -Location $location
New-AzResourceGroupDeployment -ResourceGroupName $rg -TemplateFile $file

exit

switch ($cmd) {
    
  delete {
    #az group delete --name test
    #az group create --location francecentral --name test  
  }

  logout {
    Disconnect-AzAccount
  }

  update {
    Update-Module -Name Az -Scope CurrentUser -Repository PSGallery -Force
  }

  help {
    Write-Host("Help TODO")
  }
}


