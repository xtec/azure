# TODO OneDrive sincronitza la carpeta Documents i Powershell guarda els moduls en la carpeta Documents. A l'escola no hi ha problemes, però en els ordinadors personals dels professors i els alumnes si 

# CONFIG

# Downgrade Az modules (Az.Accounts v2.12.1, Az.Resources v6.6.0)

if (-Not (Get-Command Get-AzPublicIpAddress -errorAction SilentlyContinue)) {
  Write-Host("Installing Azure Module")
  Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Scope CurrentUser -Force
  Install-Module -Name Az.Compute -Scope CurrentUser -Repository PSGallery -Force
  Install-Module -Name Az.Network -Scope CurrentUser -Repository PSGallery -Force
  Install-Module -Name Az.Resources -ModuleVersion v6.6.0 -Scope CurrentUser -Repository PSGallery -Force
}

if (-Not (Get-Command bicep -errorAction SilentlyContinue)) {
  Write-Host("Installing Bicep")
  $installPath = "$env:USERPROFILE\.bicep"
  $installDir = New-Item -ItemType Directory -Path $installPath -Force
  $installDir.Attributes += 'Hidden'
  # Fetch the latest Bicep CLI binary
        (New-Object Net.WebClient).DownloadFile("https://github.com/Azure/bicep/releases/latest/download/bicep-win-x64.exe", "$installPath\bicep.exe")
  # Add bicep to your PATH
  $currentPath = (Get-Item -path "HKCU:\Environment" ).GetValue('Path', '', 'DoNotExpandEnvironmentNames')
  if (-not $currentPath.Contains("%USERPROFILE%\.bicep")) { setx PATH ($currentPath + ";%USERPROFILE%\.bicep") }
  if (-not $env:path.Contains($installPath)) { $env:path += ";$installPath" }
}

# Creem una clau SSH si no hi ha cap

if (-not(Test-Path -Path $HOME/.ssh -PathType Container)) {
  New-Item -Path $HOME/.ssh -ItemType Directory | Out-Null
}
if (-not(Test-Path -Path $HOME\.ssh\id_rsa -PathType Leaf)) {
  Write-Host("Creant una clau SSH per autenticarte a la màquina virtual")
  # No podem crear una clau sense contrasenya sense preguntar a l'usuari fent servir la opció -N '' perquè a vegades ha de ser '""', però a vegades ha de ser 'xxxxx' mínim 5 caracters i no admet '""'. 
  ssh-keygen -m PEM -t rsa -b 4096 -f $HOME\.ssh\id_rsa
}



